using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController: ControllerBase
    {
        private readonly VendaContext _context;

        public VendasController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("CriarVenda")]
        public IActionResult Criar(Venda venda)
        {

            if(ModelState.IsValid){
                venda.Data = DateTime.Now;
                _context.Vendas.Add(venda);
                _context.SaveChanges();
                return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
            }

            return BadRequest(new { Erro = "A Venda não é válida!" });
        }

        [HttpGet("ObterPorID")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null) {
                return NotFound();
            }

            var vendedor = _context.Vendedores.Find(venda.Vendedor);
            if (vendedor == null) {
                return NotFound();
            }

            var itens = _context.Produtos.Where(x => x.Id == id).ToList();

            var v = new Venda()
            {
                Data = venda.Data,
                Vendedor = new Vendedor()
                {
                    Nome = vendedor.Nome,
                    CPF = vendedor.CPF,
                    Email = vendedor.Email,
                    Telefone = vendedor.Telefone,
                }
            };

            foreach (var produto in itens)
            {
                v.ItensVendidos.Add(new Produto
                {
                    Id = produto.Id,
                    Nome = produto.Nome,
                    Preco = produto.Preco,
                    Descricao = produto.Descricao,
                    Quantidade = produto.Quantidade
                });
            }

            return Ok(v);
        }

        [HttpPut("AtualizaStatusVenda")]
        public IActionResult AtualizaStatusVenda(int id, StatusPagamento status)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null){
                return NotFound();
            }

            if (vendaBanco.Status == StatusPagamento.AguardandoPagamento && status == StatusPagamento.PagamentoAprovado
                || vendaBanco.Status == StatusPagamento.AguardandoPagamento && status == StatusPagamento.Cancelada)
            {
                vendaBanco.Status = status;
            }
            else if (vendaBanco.Status == StatusPagamento.PagamentoAprovado && status == StatusPagamento.EnviadoParaTransportadora
                || vendaBanco.Status == StatusPagamento.PagamentoAprovado && status == StatusPagamento.Cancelada)
            {
                vendaBanco.Status = status;
            }
            else if (vendaBanco.Status == StatusPagamento.EnviadoParaTransportadora && status == StatusPagamento.Entregue)
            {
                vendaBanco.Status = status;
            }
            else
            {
                return BadRequest();
            }
           
            _context.Vendas.Update(vendaBanco); 
            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodasVendas()
        {
            var vendas = _context.Vendas.ToList();

            if (vendas == null) {
                return NotFound();
            }

            foreach (var venda in vendas){
                var vendedor = _context.Vendedores.Find(venda.VendedorId);
                
                if (vendedor == null) {
                    return NotFound();
                }

                var itens = _context.Produtos.Where(x => x.Id == venda.Id).ToList();

                var v = new Venda()
                {
                    Data = venda.Data,
                    Status = venda.Status,
                    Vendedor = new Vendedor()
                    {
                        Nome = vendedor.Nome,
                        CPF = vendedor.CPF,
                        Email = vendedor.Email,
                        Telefone = vendedor.Telefone,
                    }
                };

                foreach (var produto in itens)
                {
                    v.ItensVendidos.Add(new Produto
                    {
                        Id = produto.Id,
                        Nome = produto.Nome,
                        Preco = produto.Preco,
                        Descricao = produto.Descricao,
                        Quantidade = produto.Quantidade
                    });
                }
            };

            return Ok(vendas);
        }
    
        [HttpGet("ObterVendaPorVendedor")]
        public IActionResult ObterVendaPorVendedor(int idVendedor){

            var vendas = _context.Vendas.Where(x => x.VendedorId == idVendedor).ToList();
           
            if (vendas == null) {
                return NotFound();
            }

            foreach (var venda in vendas){
                var vendedor = _context.Vendedores.Find(venda.VendedorId);
                
                if (vendedor == null) {
                    return NotFound();
                }

                var itens = _context.Produtos.Where(x => x.Id == venda.Id).ToList();

                var v = new Venda()
                {
                    Data = venda.Data,
                    Status = venda.Status,
                    Vendedor = new Vendedor(){
                        Nome = vendedor.Nome,
                        CPF = vendedor.CPF,
                        Email = vendedor.Email,
                        Telefone = vendedor.Telefone,
                    }
                };

                foreach (var produto in itens)
                {
                    v.ItensVendidos.Add(new Produto
                    {
                        Id = produto.Id,
                        Nome = produto.Nome,
                        Preco = produto.Preco,
                        Descricao = produto.Descricao,
                        Quantidade = produto.Quantidade
                    });
                }
            };

            return Ok(vendas);
        }
    
    }
}
