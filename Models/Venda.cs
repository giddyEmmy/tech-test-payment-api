using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; } = new Vendedor();

        public int ProdutoId { get; set; }
        public int QuantidadePedido { get; set; }
        public DateTime Data { get; set; }
        public StatusPagamento Status { get; set; } = StatusPagamento.AguardandoPagamento;
        public List<Produto> ItensVendidos { get; set; }
    }
}